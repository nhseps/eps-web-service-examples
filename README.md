EPS Web Service Examples and Documentation
===========================================

The [EPS Web Services](http://nww.etpwebservices.cfh.nhs.uk/ETPWebservices/service.asmx) are hosted by NHS Choices and provide four methods:
* GetDispenserByNacsCode
* GetDispenserByName
* GetDispenserByNameAndLocation
* GetDispenserByNearest

These methods can be called by either simple HTTP POST/GET or using SOAP 1.1 or 1.2.


Web Service Description
-----------------------
The normative source of the [EPS Web Service Description](http://nww.etpwebservices.cfh.nhs.uk/ETPWebservices/service.asmx?WSDL) is provided alongside the service.

Generated documentation is provided here in the /wsdl directory.

Examples
---------
The web service provides a [friendly HTML interface](http://nww.etpwebservices.cfh.nhs.uk/ETPWebservices/service.asmx) which can quieried in a browser, or see the examples in the /example directory.

Test Service
------------
A test service providing data for the test environments should be available at http://nww.etpwebservicescompliance.cfh.nhs.uk/ETPWebservices/service.asmx. This is maintained by the EPS programme who can make changes to the data to support supplier testing. 